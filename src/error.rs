use core:: {
    fmt:: { self, Debug, Display },
};
use const_frac;

#[cfg(feature = "std")]
use std;

#[derive(Debug)]
pub enum Error {
    DimensionMismatch,
    IncompatibleValue,
    IncorrectDefinition,
    InvalidUnitString,
    FracError(const_frac::error::Error),
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        Debug::fmt(self, f)
    }
}

#[cfg(feature = "std")]
#[cfg_attr(docsrs, doc(cfg(feature = "std")))]
impl std::error::Error for Error {}