use const_frac;

#[cfg(feature = "ratio")]
use num_bigint::BigInt;
#[cfg(feature = "ratio")]
use num_rational::Ratio;

pub trait Real: const_frac::Real
{}

impl Real for f64 {}
impl Real for const_frac::Frac {}
#[cfg(feature = "ratio")]
#[cfg_attr(docsrs, doc(cfg(feature = "ratio")))]
impl Real for Ratio<BigInt>
{}