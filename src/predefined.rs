use crate::PhysicalQuantity;

pub mod dim;
#[cfg(feature = "default-units")]
#[cfg_attr(docsrs, doc(cfg(feature = "default-units")))]
pub mod unit;

pub type Dimensionless<R> = PhysicalQuantity<R, dim::Dimensionless>;

pub type Length<R> = PhysicalQuantity<R, dim::Length>;
pub type Mass<R> = PhysicalQuantity<R, dim::Mass>;
pub type Time<R> = PhysicalQuantity<R, dim::Time>;
pub type Temperature<R> = PhysicalQuantity<R, dim::Temperature>;
pub type Amount<R> = PhysicalQuantity<R, dim::Amount>;
pub type Current<R> = PhysicalQuantity<R, dim::Current>;
pub type Luminous<R> = PhysicalQuantity<R, dim::Luminous>;

pub type Frequency<R> = PhysicalQuantity<R, dim::Frequency>;
pub type FrequencyAcceleration<R> = PhysicalQuantity<R, dim::FrequencyAcceleration>;
pub type Area<R> = PhysicalQuantity<R, dim::Area>;
pub type Volume<R> = PhysicalQuantity<R, dim::Volume>;
pub type Velocity<R> = PhysicalQuantity<R, dim::Velocity>;
pub type Acceleration<R> = PhysicalQuantity<R, dim::Acceleration>;
pub type Force<R> = PhysicalQuantity<R, dim::Force>;
pub type Pressure<R> = PhysicalQuantity<R, dim::Pressure>;
pub type Head<R> = PhysicalQuantity<R, dim::Head>;
pub type Energy<R> = PhysicalQuantity<R, dim::Energy>;
pub type Power<R> = PhysicalQuantity<R, dim::Power>;
pub type Voltage<R> = PhysicalQuantity<R, dim::Voltage>;
pub type ElectricFieldStrength<R> = PhysicalQuantity<R, dim::ElectricFieldStrength>;
pub type ElectricResistance<R> = PhysicalQuantity<R, dim::ElectricResistance>;
pub type ElectricConductance<R> = PhysicalQuantity<R, dim::ElectricConductance>;
pub type ElectricalInductance<R> = PhysicalQuantity<R, dim::ElectricalInductance>;
pub type ElectricCharge<R> = PhysicalQuantity<R, dim::ElectricCharge>;
pub type Capacitance<R> = PhysicalQuantity<R, dim::Capacitance>;
pub type RadiationExposure<R> = PhysicalQuantity<R, dim::RadiationExposure>;
pub type MagneticFlux<R> = PhysicalQuantity<R, dim::MagneticFlux>;
pub type MagneticFluxDensity<R> = PhysicalQuantity<R, dim::MagneticFluxDensity>;
pub type MagneticFieldStrength<R> = PhysicalQuantity<R, dim::MagneticFieldStrength>;
pub type Illuminance<R> = PhysicalQuantity<R, dim::Illuminance>;
pub type Viscosity<R> = PhysicalQuantity<R,dim::Viscosity>;
pub type KinematicViscosity<R> = PhysicalQuantity<R,dim::KinematicViscosity>;
pub type Density<R> = PhysicalQuantity<R,dim::Density>;
