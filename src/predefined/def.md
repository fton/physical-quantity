| name | a | b | dim | remarks |
|------|---|---|-----|---------|
| ppq | 1e-15 | — | — |  |
| ppt | 1e-12 | — | — |  |
| ppb | 1e-9 | — | — |  |
| ppm | 1e-6 | — | — |  |
| ㏙ | 1e-6 | — | — |  `\u{33D9}`: ppm in CJK compatibility character. |
| ‱ | 1e-4 | — | — |  `\u{2031}`: [Permyriad](https://en.wikipedia.org/wiki/Basis_point) |
| ‰ | 0.001 | — | — |  `\u{2030}`: [Per mille](https://en.wikipedia.org/wiki/Per_mille) |
| ° | 0.0027777778 | — | — |  `\u{00B0}`: Degree (angle). |
| ˚ | 0.0027777778 | — | — |  `\u{02DA}`: Degree (angle). |
| % | 0.01 | — | — |  |
| ㌫ | 0.01 | — | — |  `\u{332B}`: Percent in CJK compatibility character. |
| sr | 0.0795774715 | — | — |  |
| ㏛ | 0.0795774715 | — | — |  `\u{33DB}`: Steradian in CJK compatibility character. |
| rad | 0.1591549431 | — | — |  |
| ㎭ | 0.1591549431 | — | — |  `\u{33AD}`: Radian in CJK compatibility character. |
| ¹ | 1.0 | — | — |  `\u{00B9}`: SUPERSCRIPT ONE. \"1/s\", same as \"Hz\", cannot accept as unit string because the digit one (1`\u{0031}`) cannot compose the _Name_. Using superscript one and fraction slash (⁄`\u{2044}`), \"1/s\" can be written \"¹⁄s\". This character sequence may be displayed more preferable way in most software. |
| ‐ | 1.0 | — | — |  `\u{2010}`: Hyphen. (See [Dimensionless](../unit/index.html#dimensionless)) |
| ‒ | 1.0 | — | — |  `\u{2012}`: Figure dash. (See [Dimensionless](../unit/index.html#dimensionless)) |
| – | 1.0 | — | — |  `\u{2013}`: EN dash. (See [Dimensionless](../unit/index.html#dimensionless)) |
| — | 1.0 | — | — |  `\u{2014}`: EM DASH. (See [Dimensionless](../unit/index.html#dimensionless)) |
| ― | 1.0 | — | — |  `\u{2015}`: HORIZONTAL BAR. (See [Dimensionless](../unit/index.html#dimensionless)) |
| ㌟ | 1.0 | — | — |  `\u{331F}`: Cycle in CJK compatibility character. |
| ㎙ | 1e-12 | — | m1 |  `\u{3399}`: Femto metre in CJK compatibility character. |
| Å | 1e-10 | — | m1 |  `\u{00C5}`: [Angstrom](https://en.wikipedia.org/wiki/Angstrom) |
| Å | 1e-10 | — | m1 |  `\u{212B}`: [Angstrom](https://en.wikipedia.org/wiki/Angstrom) |
| ㎚ | 1e-9 | — | m1 |  `\u{339A}`: Nano metre in CJK compatibility character. |
| ㎛ | 1e-6 | — | m1 |  `\u{339B}`: Micro metre in CJK compatibility character. |
| ㎜ | 0.001 | — | m1 |  `\u{339C}`: Milli metre in CJK compatibility character. |
| ㎝ | 0.01 | — | m1 |  `\u{339D}`: Centi metre in CJK compatibility character. |
| in. | 0.0254 | — | m1 |  Inch. The period is needed to prevent recognizing 'min' as milli inch. |
| ㌅ | 0.0254 | — | m1 |  `\u{3305}`: Inch in CJK compatibility character. |
| ㏌ | 0.0254 | — | m1 |  `\u{33CC}`: Inch in CJK compatibility character. |
| ㍷ | 0.1 | — | m1 |  `\u{3377}`: Deci metre in CJK compatibility character. |
| ft. | 0.3048 | — | m1 |  The period is needed to prevent conflicting with femto ton (ft) |
| ㌳ | 0.3048 | — | m1 |  `\u{3333}`: Foot in CJK compatibility character. |
| yd. | 0.9144 | — | m1 |  Yard. Because foot and inch need a period, the yard also requires it to prevent confusion. |
| ㍎ | 0.9144 | — | m1 |  `\u{334E}`: Yard in CJK compatibility character. |
| ㍏ | 0.9144 | — | m1 |  `\u{334F}`: Yard in CJK compatibility character. |
| m | 1.0 | — | m1 |  Metre (meter). The base unit of length. |
| ㍍ | 1.0 | — | m1 |  `\u{334D}`: Meter in CJK compatibility character. |
| ㌖ | 1000.0 | — | m1 |  `\u{3316}`: Kilo metre in CJK compatibility character. |
| ㎞ | 1000.0 | — | m1 |  `\u{339E}`: Kilo metre in CJK compatibility character. |
| mile | 1609.344 | — | m1 |  |
| ㍄ | 1609.344 | — | m1 |  `\u{3344}`: Mile in CJK compatibility character. |
| ㏕ | 1609.344 | — | m1 |  `\u{33D5}`: Mile in CJK compatibility character. |
| ㌋ | 1852.0 | — | m1 |  `\u{330B}`: Nautical mile in CJK compatibility character. |
| 海里 | 1852.0 | — | m1 |  `\u{6D77}\u{91CC}`: Nautical mile in hanzi (kanji). |
| au | 1.4959787e11 | — | m1 |  [Astronomical unit](https://en.wikipedia.org/wiki/Astronomical_unit). |
| ㍳ | 1.4959787e11 | — | m1 |  `\u{3373}`: [Astronomical unit](https://en.wikipedia.org/wiki/Astronomical_unit) in CJK compatibility character. |
| pc. | 3.0856776e16 | — | m1 |  [Parsec](https://en.wikipedia.org/wiki/Parsec). The period is needed to prevent conflicting the light speed (`c`). |
| ㍶ | 3.0856776e16 | — | m1 |  `\u{3376}`: [Parsec](https://en.wikipedia.org/wiki/Parsec) in CJK compatibility character. |
| ㎟ | 1e-6 | — | m2 |  `\u{339F}`: mm<sup>2</sup> in CJK compatibility character. |
| ㎠ | 1e-4 | — | m2 |  `\u{33A0}`: cm<sup>2</sup> in CJK compatibility character. |
| ㍸ | 0.01 | — | m2 |  `\u{3378}`: dm<sup>2</sup> in CJK compatibility character. |
| ㎡ | 1.0 | — | m2 |  `\u{33A1}`: m<sup>2</sup> in CJK compatibility character. |
| a. | 100.0 | — | m2 |  Are. To prevent conflicting Pascal (Pa), it needs a period. |
| а | 100.0 | — | m2 |  `\u{0430}`: CYRILLIC SMALL LETTER A. If you really don't want to put a period in are, use this one. |
| ㌃ | 100.0 | — | m2 |  `\u{3303}`: Are in CJK compatibility character. |
| acre | 4046.8564224 | — | m2 |  |
| ㌈ | 4046.8564224 | — | m2 |  `\u{3308}`: Acre in CJK compatibility character. |
| ㌶ | 10000.0 | — | m2 |  `\u{3336}`: Hecto are in CJK compatibility character. |
| ㏊ | 10000.0 | — | m2 |  `\u{33CA}`: Hecto are in CJK compatibility character. |
| ㎢ | 1e6 | — | m2 |  `\u{33A2}`: km<sup>2</sup> in CJK compatibility character. |
| ㎕ | 1e-9 | — | m3 |  `\u{3395}`: Micro litre in CJK compatibility character. |
| ㎣ | 1e-9 | — | m3 |  `\u{33A3}`: mm<sup>3</sup> in CJK compatibility character. |
| ㎖ | 1e-6 | — | m3 |  `\u{3396}`: Milli litre in CJK compatibility character. |
| ㎤ | 1e-6 | — | m3 |  `\u{33A4}`: cm<sup>3</sup> in CJK compatibility character. |
| ㏄ | 1e-6 | — | m3 |  `\u{33C4}`: Cubic centimeter in CJK compatibility character. |
| ㎗ | 1e-4 | — | m3 |  `\u{3397}`: Deci litre in CJK compatibility character. |
| L | 0.001 | — | m3 |  Litre (Liter) |
| l | 0.001 | — | m3 |  Litre (Liter) |
| ℓ | 0.001 | — | m3 |  `\u{2113}`: Litre in CJK compatibility character. |
| ㍹ | 0.001 | — | m3 |  `\u{3379}`: dm<sup>3</sup> in CJK compatibility character. |
| gal.us | 0.003785412 | — | m3 |  U.S. fluid gallon |
| ㌎ | 0.003785412 | — | m3 |  `\u{330E}`: U.S. fluid gallon in CJK compatibility character. |
| gal.en | 0.00454609 | — | m3 |  Imperial gallon |
| bbl | 0.1589872949 | — | m3 |  Barrel |
| ㌭ | 0.1589872949 | — | m3 |  `\u{332D}`: Barrel in CJK compatibility character. |
| ㎘ | 1.0 | — | m3 |  `\u{3398}`: Kilo litre in CJK compatibility character. |
| ㎥ | 1.0 | — | m3 |  `\u{33A5}`: m<sup>3</sup> in CJK compatibility character. |
| ㎦ | 1e9 | — | m3 |  `\u{33A6}`: km<sup>3</sup> in CJK compatibility character. |
| Da | 1.660539e-24 | — | g1 |  [Dalton](https://en.wikipedia.org/wiki/Dalton_(unit)). This definition exclude `u`(unified atomic mass unit) to prevent conflicting [Astronomical unit](https://en.wikipedia.org/wiki/Astronomical_unit) (`au`). |
| ㎍ | 1e-6 | — | g1 |  `\u{338D}`: Micro gram in CJK compatibility character. |
| ㎎ | 0.001 | — | g1 |  `\u{338E}`: Milli gram in CJK compatibility character. |
| ㌌ | 0.2 | — | g1 |  `\u{330C}`: [Carat](https://en.wikipedia.org/wiki/Carat_(mass)) in CJK compatibility character. |
| g | 1.0 | — | g1 |  Gram. The base unit of mass. The [SI Standards] adopts the kilogram (kg) as the base unit of mass. But this definition adopts the gram (g) for ease of parse. |
| ㌘ | 1.0 | — | g1 |  `\u{3318}`: Gram in CJK compatibility character. |
| oz | 28.349523125 | — | g1 |  Ounce |
| ㌉ | 28.349523125 | — | g1 |  `\u{3309}`: Ounce in CJK compatibility character. |
| lb | 453.59237 | — | g1 |  |
| ㍀ | 453.59237 | — | g1 |  `\u{3340}`: Pound in CJK compatibility character. |
| ㌕ | 1000.0 | — | g1 |  `\u{3315}`: Kilo gram in CJK compatibility character. |
| ㎏ | 1000.0 | — | g1 |  `\u{338F}`: Kilo gram in CJK compatibility character. |
| t | 1e6 | — | g1 |  |
| ㌙ | 1e6 | — | g1 |  `\u{3319}`: Metric ton in CJK compatibility character. |
| ㌧ | 1e6 | — | g1 |  `\u{3327}`: Tonne in CJK compatibility character. Same as metric ton. |
| ㍌ | 1e9 | — | g1 |  `\u{334C}`: Megatonne in CJK compatibility character. |
| ㎰ | 1e-12 | — | s1 |  `\u{33B0}`: Pico second in CJK compatibility character. |
| ㎱ | 1e-9 | — | s1 |  `\u{33B1}`: Nano second in CJK compatibility character. |
| ㎲ | 1e-6 | — | s1 |  `\u{33B2}`: Micro second in CJK compatibility character. |
| ㎳ | 0.001 | — | s1 |  `\u{33B3}`: Milli second in CJK compatibility character. |
| s | 1.0 | — | s1 |  Second. Base unit of Time. |
| 秒 | 1.0 | — | s1 |  `\u{79D2}`: Second in hanzi (kanji). |
| min | 60.0 | — | s1 |  Minute. |
| 分 | 60.0 | — | s1 |  `\u{5206}`: Minute in hanzi (kanji). |
| h | 3600.0 | — | s1 |  Hour. |
| 時 | 3600.0 | — | s1 |  `\u{6642}`: Hour in hanzi (kanji). |
| day | 86400.0 | — | s1 |  Day. |
| 日 | 86400.0 | — | s1 |  `\u{65E5}`: Day in hanzi (kanji). |
| year | 3.15576e7 | — | s1 |  [Julian year](https://en.wikipedia.org/wiki/Julian_year_(astronomy)) |
| 年 | 3.15576e7 | — | s1 |  `\u{5E74}`: Year in hanzi (kanji). |
| ㎺ | 1e-15 | — | m2g1s-3 |  `\u{33BA}`: Pico Watt in CJK compatibility character. |
| ㎻ | 1e-12 | — | m2g1s-3 |  `\u{33BB}`: Nano Watt in CJK compatibility character. |
| ㎼ | 1e-9 | — | m2g1s-3 |  `\u{33BC}`: Micro Watt in CJK compatibility character. |
| ㎽ | 1e-6 | — | m2g1s-3 |  `\u{33BD}`: Milli Watt in CJK compatibility character. |
| W | 1000.0 | — | m2g1s-3 |  Watt. |
| ㍗ | 1000.0 | — | m2g1s-3 |  `\u{3357}`: Watt in CJK compatibility character. |
| ㏋ | 7.45699872e5 | — | m2g1s-3 |  `\u{33CB}`: Imperial horsepower in CJK compatibility character. |
| ㌗ | 1e6 | — | m2g1s-3 |  `\u{3317}`: Kilo Watt in CJK compatibility character. |
| ㎾ | 1e6 | — | m2g1s-3 |  `\u{33BE}`: Kilo Watt in CJK compatibility character. |
| ㎿ | 1e9 | — | m2g1s-3 |  `\u{33BF}`: Mega Watt in CJK compatibility character. |
| ㎯ | 0.1591549431 | — | s-2 |  `\u{33AF}`: rad/s<sup>2</sup> in CJK compatibility character. |
| ㎨ | 1.0 | — | m1s-2 |  `\u{33A8}`: m/s<sup>2</sup> in CJK compatibility character. |
| ㍕ | 10.0 | — | m2s-2 |  `\u{3355}`: [Rem](https://en.wikipedia.org/wiki/Roentgen_equivalent_man) in CJK compatibility character. |
| Gy | 1000.0 | — | m2s-2 |  [Gray](https://en.wikipedia.org/wiki/Gray_(unit)). |
| Sv | 1000.0 | — | m2s-2 |  [Sievert](https://en.wikipedia.org/wiki/Sievert). |
| ㏉ | 1000.0 | — | m2s-2 |  `\u{33C9}`: [Gray](https://en.wikipedia.org/wiki/Gray_(unit)) in CJK compatibility character. |
| ㏜ | 1000.0 | — | m2s-2 |  `\u{33DC}`: [Sievert](https://en.wikipedia.org/wiki/Sievert) in CJK compatibility character. |
| gf | 9.80665 | — | m1g1s-2 |  Gram force. |
| N | 1000.0 | — | m1g1s-2 |  Newton |
| lbf | 4448.2216153 | — | m1g1s-2 |  Pound force. |
| eV | 1.602177e-16 | — | m2g1s-2 |  [Electronvolt](https://en.wikipedia.org/wiki/Electronvolt). |
| ㋎ | 1.602177e-16 | — | m2g1s-2 |  `\u{32CE}`: [Electronvolt](https://en.wikipedia.org/wiki/Electronvolt) in CJK compatibility character. |
| E_h | 4.359745e-15 | — | m2g1s-2 |  [Hartree atomic units](https://en.wikipedia.org/wiki/Hartree_atomic_units). |
| erg | 1e-4 | — | m2g1s-2 |  [Erg](https://en.wikipedia.org/wiki/Erg). |
| ㋍ | 1e-4 | — | m2g1s-2 |  `\u{32CD}`: [Erg](https://en.wikipedia.org/wiki/Erg) in CJK compatibility character. |
| J | 1000.0 | — | m2g1s-2 |  [Joule](https://en.wikipedia.org/wiki/Joule). |
| cal | 4186.8 | — | m2g1s-2 |  [Calorie](https://en.wikipedia.org/wiki/Calorie). |
| ㌍ | 4186.8 | — | m2g1s-2 |  `\u{330D}`: [Calorie](https://en.wikipedia.org/wiki/Calorie) in CJK compatibility character. |
| ㎈ | 4186.8 | — | m2g1s-2 |  `\u{3388}`: [Calorie](https://en.wikipedia.org/wiki/Calorie) in CJK compatibility character. |
| Btu | 1.05505585e6 | — | m2g1s-2 |  [British thermal unit](https://en.wikipedia.org/wiki/British_thermal_unit). |
| ㎉ | 4.1868e6 | — | m2g1s-2 |  `\u{3389}`: Kilo [Calorie](https://en.wikipedia.org/wiki/Calorie) in CJK compatibility character. |
| Hg | 1.33322368e5 | — | m-2g1s-2 |  Pressure based mercury head. lacked length dimension. (see [Head](../unit/index.html#head)) |
| ㋌ | 1.33322368e5 | — | m-2g1s-2 |  `\u{32CC}`: Hg in CJK compatibility character. (see [Head](../unit/index.html#head)) |
| Aq | 9.80665e6 | — | m-2g1s-2 |  Pressure based water head. lacked length dimension. (see [Head](../unit/index.html#head)) |
| Pa | 1000.0 | — | m-1g1s-2 |  Pascal. |
| ㎩ | 1000.0 | — | m-1g1s-2 |  `\u{33A9}`: Pascal in CJK compatibility character. |
| ㍊ | 1e5 | — | m-1g1s-2 |  `\u{334A}`: Milli bar in CJK compatibility character. |
| ㍱ | 1e5 | — | m-1g1s-2 |  `\u{3371}`: Hecto Pascal in CJK compatibility character. |
| ㏔ | 1e5 | — | m-1g1s-2 |  `\u{33D4}`: Milli bar in CJK compatibility character. |
| ㎪ | 1e6 | — | m-1g1s-2 |  `\u{33AA}`: Kilo Pascal in CJK compatibility character. |
| psi | 6.89475729e6 | — | m-1g1s-2 |  Pound force per square inch (lbf/in.<sup>2</sup>). |
| psia | 6.89475729e6 | — | m-1g1s-2 |  Absolute pound force per square inch (lbf/in.<sup>2</sup>\[abs\]). |
| psig | 6.89475729e6 | 1.01325e8 | m-1g1s-2 |  Gaged pound force per square inch (lbf/in.<sup>2</sup>\[gage\]). |
| at. | 9.80665e7 | — | m-1g1s-2 |  at (kgf/cm<sup>2</sup>). It needs period to prevent conflicting atto ton (at). |
| ata | 9.80665e7 | — | m-1g1s-2 |  Absolute at. Same as kgf/cm<sup>2</sup>\[abs\]. |
| atg | 9.80665e7 | 1.01325e8 | m-1g1s-2 |  Gaged at. Same as kgf/cm<sup>2</sup>\[gage\]. |
| bar | 1e8 | — | m-1g1s-2 |  |
| ㍴ | 1e8 | — | m-1g1s-2 |  `\u{3374}`: bar in CJK compatibility character. |
| ㎫ | 1e9 | — | m-1g1s-2 |  `\u{33AB}`: Mega Pascal in CJK compatibility character. |
| ㎬ | 1e12 | — | m-1g1s-2 |  `\u{33AC}`: Giga Pascal in CJK compatibility character. |
| rpm | 0.0166666667 | — | s-1 |  |
| ㎮ | 0.1591549431 | — | s-1 |  `\u{33AE}`: rad/s in CJK compatibility character. |
| Bq | 1.0 | — | s-1 |  [Becquerel](https://en.wikipedia.org/wiki/Becquerel). |
| Hz | 1.0 | — | s-1 |  |
| ㏃ | 1.0 | — | s-1 |  `\u{33C3}`: [Becquerel](https://en.wikipedia.org/wiki/Becquerel) in CJK compatibility character. |
| Ci | 3.7e10 | — | s-1 |  [Curie](https://en.wikipedia.org/wiki/Curie_(unit)). |
| ㌒ | 3.7e10 | — | s-1 |  `\u{3312}`: [Curie](https://en.wikipedia.org/wiki/Curie_(unit)) in CJK compatibility character. |
| ㌹ | 1e12 | — | s-1 |  `\u{3339}`: Hz in CJK compatibility character. |
| Gal | 0.01 | — | m1s-1 |  [Gal](https://en.wikipedia.org/wiki/Gal_(unit)). Not gallon. |
| ㏿ | 0.01 | — | m1s-1 |  `\u{33FF}`: [Gal](https://en.wikipedia.org/wiki/Gal_(unit)) in CJK compatibility character. |
| kn | 0.5144444444 | — | m1s-1 |  Knot. This definition exclude `kt` to prevent conflicting with kilo ton (kt). |
| ㌩ | 0.5144444444 | — | m1s-1 |  `\u{3329}`: Knot in CJK compatibility character. |
| ㏏ | 0.5144444444 | — | m1s-1 |  `\u{33CF}`: Knot in CJK compatibility character. |
| ㎧ | 1.0 | — | m1s-1 |  `\u{33A7}`: m/s in CJK compatibility character. |
| c | 2.99792458e8 | — | m1s-1 |  The speed of light in vacuum. |
| light | 2.99792458e8 | — | m1s-1 |  The speed of light in vacuum. This can describe astronomical length by combining with time units. (e.g. \"light year\") |
| 光 | 2.99792458e8 | — | m1s-1 |  `\u{5149}`: The speed of light in vacuum. This can describe astronomical length by combining with time units. Hanji (kanji) does not use a space character to delimit word ordinary. Zero width space is suitable for this use. (e.g. 光​年) |
| St | 1e-4 | — | m2s-1 |  [Stokes](https://en.wikipedia.org/wiki/Stokes_(unit)). |
| P | 100.0 | — | m-1g1s-1 |  [Poise](https://en.wikipedia.org/wiki/Poise_(unit)). |
| °R | 0.5555555556 | — | K1 |  `\u{00B0}` and `R`: The [degrees Rankine](https://en.wikipedia.org/wiki/Rankine_scale). |
| °F | 0.5555555556 | 7273.1944444 | K1 |  `\u{00B0}` and `F`: The [degree Fahrenheit](https://en.wikipedia.org/wiki/Fahrenheit). |
| ℉ | 0.5555555556 | 7273.1944444 | K1 |  `\u{2109}`: The [degree Fahrenheit](https://en.wikipedia.org/wiki/Fahrenheit). |
| K | 1.0 | — | K1 |  [Kelvin](https://en.wikipedia.org/wiki/Kelvin). The base unit of temperature. |
| K | 1.0 | — | K1 |  `\u{212A}`: [Kelvin](https://en.wikipedia.org/wiki/Kelvin). The base unit of temperature. |
| °C | 1.0 | 273.15 | K1 |  `\u{00B0}` and `C`: The [degree Celsius](https://en.wikipedia.org/wiki/Celsius). |
| ℃ | 1.0 | 273.15 | K1 |  `\u{2103}`: The [degree Celsius](https://en.wikipedia.org/wiki/Celsius). |
| mol | 1.0 | — | mol1 |  Mole. The base unit of amount of substance. |
| ㏖ | 1.0 | — | mol1 |  `\u{33D6}`: Mole in CJK compatibility character. |
| ㎀ | 1e-12 | — | A1 |  `\u{3380}`: Pico ampere in CJK compatibility character. |
| ㎁ | 1e-9 | — | A1 |  `\u{3381}`: Nano ampere in CJK compatibility character. |
| ㎂ | 1e-6 | — | A1 |  `\u{3382}`: Micro ampere in CJK compatibility character. |
| ㎃ | 0.001 | — | A1 |  `\u{3383}`: Milli ampere in CJK compatibility character. |
| A | 1.0 | — | A1 |  [Ampere](https://en.wikipedia.org/wiki/Ampere). The base unit of electric current. |
| ㌂ | 1.0 | — | A1 |  `\u{3302}`: Ampere in CJK compatibility character. |
| ㎄ | 1000.0 | — | A1 |  `\u{3384}`: Kilo ampere in CJK compatibility character. |
| ㏟ | 1.0 | — | m-1A1 |  `\u{33DF}`: Ampere par metre in CJK compatibility character. |
| C | 1.0 | — | s1A1 |  [Coulomb](https://en.wikipedia.org/wiki/Coulomb). |
| ㏆ | 0.001 | — | g-1s1A1 |  `\u{33C6}`: C/kg in CJK compatibility character. |
| ㍖ | 0.00258 | — | g-1s1A1 |  `\u{3356}`: [Roentgen](https://en.wikipedia.org/wiki/Roentgen_(unit)) in CJK compatibility character. |
| S | 0.001 | — | m-2g-1s3A2 |  [Siemens](https://en.wikipedia.org/wiki/Siemens_(unit)). |
| ℧ | 0.001 | — | m-2g-1s3A2 |  `\u{2127}`: [Siemens](https://en.wikipedia.org/wiki/Siemens_(unit)). |
| ㎊ | 1e-15 | — | m-2g-1s4A2 |  `\u{338A}`: Pico farad in CJK compatibility character. |
| ㎋ | 1e-12 | — | m-2g-1s4A2 |  `\u{338B}`: Nano farad in CJK compatibility character. |
| ㎌ | 1e-9 | — | m-2g-1s4A2 |  `\u{338C}`: Micro farad in CJK compatibility character. |
| F | 0.001 | — | m-2g-1s4A2 |  [Farad](https://en.wikipedia.org/wiki/Farad). |
| ㌲ | 0.001 | — | m-2g-1s4A2 |  `\u{3332}`: [Farad](https://en.wikipedia.org/wiki/Farad) in CJK compatibility character. |
| Ω | 1000.0 | — | m2g1s-3A-2 |  `\u{03A9}`: Ohm. |
| Ω | 1000.0 | — | m2g1s-3A-2 |  `\u{2126}`: Ohm. |
| ㌊ | 1000.0 | — | m2g1s-3A-2 |  `\u{330A}`: Ohm in CJK compatibility character. |
| ㏀ | 1e6 | — | m2g1s-3A-2 |  `\u{33C0}`: Kilo ohm in CJK compatibility character. |
| ㏁ | 1e9 | — | m2g1s-3A-2 |  `\u{33C1}`: Mega ohm in CJK compatibility character. |
| H | 1000.0 | — | m2g1s-2A-2 |  [Henry](https://en.wikipedia.org/wiki/Henry_(unit)). |
| ㏞ | 1000.0 | — | m1g1s-3A-1 |  `\u{33DE}`: V/m in CJK compatibility character. |
| ㎴ | 1e-9 | — | m2g1s-3A-1 |  `\u{33B4}`: Pico volt in CJK compatibility character. |
| ㎵ | 1e-6 | — | m2g1s-3A-1 |  `\u{33B5}`: Nano volt in CJK compatibility character. |
| ㎶ | 0.001 | — | m2g1s-3A-1 |  `\u{33B6}`: Micro volt in CJK compatibility character. |
| ㎷ | 1.0 | — | m2g1s-3A-1 |  `\u{33B7}`: Milli volt in CJK compatibility character. |
| V | 1000.0 | — | m2g1s-3A-1 |  Volt. |
| ㌾ | 1000.0 | — | m2g1s-3A-1 |  `\u{333E}`: Volt in CJK compatibility character. |
| ㎸ | 1e6 | — | m2g1s-3A-1 |  `\u{33B8}`: Kilo volt in CJK compatibility character. |
| ㎹ | 1e9 | — | m2g1s-3A-1 |  `\u{33B9}`: Mega volt in CJK compatibility character. |
| T | 1000.0 | — | g1s-2A-1 |  [Tesla](https://en.wikipedia.org/wiki/Tesla_(unit)). |
| Wb | 1000.0 | — | m2g1s-2A-1 |  [Weber](https://en.wikipedia.org/wiki/Weber_(unit)). |
| ㏝ | 1000.0 | — | m2g1s-2A-1 |  `\u{33DD}`: [Weber](https://en.wikipedia.org/wiki/Weber_(unit)) in CJK compatibility character. |
| lm | 0.0795774715 | — | cd1 |  [Lumen](https://en.wikipedia.org/wiki/Lumen_(unit)). |
| ㏐ | 0.0795774715 | — | cd1 |  `\u{33D0}`: [Lumen](https://en.wikipedia.org/wiki/Lumen_(unit)) in CJK compatibility character. |
| cd | 1.0 | — | cd1 |  [Candela](https://en.wikipedia.org/wiki/Candela). The base unit of luminous intensity. |
| ㏅ | 1.0 | — | cd1 |  `\u{33C5}`: [Candela](https://en.wikipedia.org/wiki/Candela) in CJK compatibility character. |
| lx | 0.0795774715 | — | m-2cd1 |  [Lux](https://en.wikipedia.org/wiki/Lux). |
| ㏓ | 0.0795774715 | — | m-2cd1 |  `\u{33D3}`: [Lux](https://en.wikipedia.org/wiki/Lux) in CJK compatibility character. |
