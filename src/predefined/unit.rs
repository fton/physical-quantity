//! Default unit definition.
//!
//! # Base Unit
//! This definition uses the following units as base units.
//!
//! - **`Length`** Metre (m)
//! - **`Mass`** Gram (g)
//! - **`Time`** Second (s)
//! - **`Temperature`** Kelvin (K)
//! - **`Amount`** Mole (mol)
//! - **`Current`** Ampere (A)
//! - **`Luminous`** Candela (cd)
//!
//! Only mass base unit is different from the [SI standards] for ease of parse.
//!
//! # Dimensionless
//! This definition defines some hyphen-like letters to represent the dimensionless dimension
//! because the minus sign (-`\u{002D}`) cannot compose the _Name_.
//!
//! # Angle
//! This definition defines one rotation as one (1).
//!
//! # Head
//! Some units represent pressure as the hight of liquid column.
//! This definition defines `Hg`(mercury) and `Aq`(water).
//! These units are given Pressure / Length dimension.
//! To use these units for pressure unit, multiply length unit.
//! (e.g. "mm Hg", "in. Aq")
//! These units are written with no spaces as "mmHg", "inHg" ordinary.
//! If these spaces are bothering you, zero width space can help.
//! (e.g. "mm​Hg", "in.​Aq")
//!
//! # Deprecated But Acceptable
//! Unicode has many curious characters which represent units.
//! These characters are deprecated because existing only for compatibility.
//! However, this definition adopts these characters positively.
//! Why? Just fun, that's all.
//!
//! # Illegal But Acceptable
//! The [SI Standards] does not allow a orphan _Prefix_.
//! This definition also.
//! But having said that, **_any_** _Name_ can promote with _Prefix_.
//! Yes, we can put the _Prefix_ to the dimensionless dimension!
//! (e.g. "M—" (Mega dimensionless??))
//! This is useful for expressing notations such as "x10<sup>6</sup>".
//!
//! A similar case is a double _Prefix_ , which is not allowed in the [SI Standard].
//! Of course, the syntax of this package implements disallows it also,
//! but some CJK compatible characters which represent the unit include a _Prefix_ already.
//! As a result of this, this definition allows some double prefix.
//! (e.g. "c㎎" (centi milli gram?? (= 10<sup>-5</sup>g)))
//! I can't think of a use for it, though.
//!
//! [SI Standards]: https://www.bipm.org/en/home
#![allow(unused_doc_comments)]
use const_array_attrs::sorted;
use const_frac::Frac;
use crate:: {
    DynDim,
    unit::Conv,
    predefined::dim::*,
};

const ZERO: Frac = Frac::from_int(0);
const ONE: Frac = Frac::from_int(1);
const RAD2BASE: Conv = Conv(Frac::from_ratio(1, 2, 0).div(Frac::from_exp_pi(1)), ZERO);
//const RAD2BASE: Conv = Conv(Frac::new(false, 1, 0xEC58_DFA7_4641_AF52_AD0D_16E7_7D57_6623, 37, 38), ZERO);
//const SR2BASE: Conv = Conv(Frac::new(false, 1, 1, 2, 0, 1), ZERO);
const SR2BASE: Conv = Conv(Frac::from_ratio(1, 4, 0).div(Frac::from_exp_pi(1)), ZERO);
const INCH2METER: Conv = Conv(Frac::from_ratio(254, 1, -4), ZERO);
const FOOT2INCH: Conv = Conv(Frac::from_int(12), ZERO);
const YARD2FOOT: Conv = Conv(Frac::from_int(3), ZERO);
const POUND2GRAM: Conv = Conv(Frac::from_ratio(453_59237, 1, -5), ZERO);
const FIVE_PAR_NINE: Frac = Frac::from_ratio(5, 9, 0);
const RANKINE2KELVIN: Conv = Conv(FIVE_PAR_NINE, ZERO);
const FAHRENHEIT2CELSIUS: Conv = Conv(FIVE_PAR_NINE, Frac::from_ratio(32 * 5, 9, 0));
const CELSIUS2KELVIN: Conv = Conv(ONE, Frac::from_ratio(273_15, 1, -2));
const NEWTON2BASE: Conv = Conv(Frac::from_exp10(3), ZERO);
const GRAM_FORCE2NEWTON: Conv = Conv(Frac::from_ratio(9_80665, 1, -8), ZERO);
const JOULE2BASE: Conv = Conv(Frac::from_exp10(3), ZERO);
/// International steam table calorie
const CALORIE2JOULE: Conv = Conv(Frac::from_ratio(4_1868, 1, -4), ZERO);
const BTU2CALORIE: Conv = POUND2GRAM.then(RANKINE2KELVIN);
const WATT2BASE: Conv = Conv(Frac::from_exp10(3), ZERO);
/// Area
const ACRE2FT2: Conv = Conv(Frac::from_ratio(4 * 40 * 165 * 165, 100, 0), ZERO);
/// pressure
const AT2BASE: Conv = GRAM_FORCE2NEWTON.mul(NEWTON2BASE).mul(Conv(Frac::from_exp10(7), ZERO));
const PSI2BASE: Conv = POUND2GRAM.mul(GRAM_FORCE2NEWTON).mul(NEWTON2BASE).div(INCH2METER.mul(INCH2METER));

#[sorted]
/// Default unit definition table.
///
#[doc = include_str!("def.md")]
pub const DEFAULT_UNIT_DEF: [(&str, (Conv, DynDim)); 231] = [
    // dimensionless

    // ‐
    /// `\u{2010}`: Hyphen.
    /// (See [Dimensionless](../unit/index.html#dimensionless))
    ("\u{2010}", (Conv(ONE, ZERO), DynDim::new(Dimensionless::CODE))),
    // ‒
    /// `\u{2012}`: Figure dash.
    /// (See [Dimensionless](../unit/index.html#dimensionless))
    ("\u{2012}", (Conv(ONE, ZERO), DynDim::new(Dimensionless::CODE))),
    // –
    /// `\u{2013}`: EN dash.
    /// (See [Dimensionless](../unit/index.html#dimensionless))
    ("\u{2013}", (Conv(ONE, ZERO), DynDim::new(Dimensionless::CODE))),
    // —
    /// `\u{2014}`: EM DASH.
    /// (See [Dimensionless](../unit/index.html#dimensionless))
    ("\u{2014}", (Conv(ONE, ZERO), DynDim::new(Dimensionless::CODE))),
    // ―
    /// `\u{2015}`: HORIZONTAL BAR.
    /// (See [Dimensionless](../unit/index.html#dimensionless))
    ("\u{2015}", (Conv(ONE, ZERO), DynDim::new(Dimensionless::CODE))),
    // ¹
    /// `\u{00B9}`: SUPERSCRIPT ONE.
    ///
    /// "1/s", same as "Hz", cannot accept as unit string
    /// because the digit one (1`\u{0031}`) cannot compose the _Name_.
    /// Using superscript one and fraction slash (⁄`\u{2044}`), "1/s" can be written "¹⁄s".
    /// This character sequence may be displayed more preferable way in most software.
    ("\u{00B9}", (Conv(ONE, ZERO), DynDim::new(Dimensionless::CODE))),
    ("%", (Conv(Frac::from_exp10(-2), ZERO), DynDim::new(Dimensionless::CODE))),
    // ㌫
    /// `\u{332B}`: Percent in CJK compatibility character.
    ("\u{332B}", (Conv(Frac::from_exp10(-2), ZERO), DynDim::new(Dimensionless::CODE))),
    // ‰
    /// `\u{2030}`: [Per mille](https://en.wikipedia.org/wiki/Per_mille)
    ("\u{2030}", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(Dimensionless::CODE))),
    // ‱
    /// `\u{2031}`: [Permyriad](https://en.wikipedia.org/wiki/Basis_point)
    ("\u{2031}", (Conv(Frac::from_exp10(-4), ZERO), DynDim::new(Dimensionless::CODE))),
    ("ppm", (Conv(Frac::from_exp10(-6), ZERO), DynDim::new(Dimensionless::CODE))),
    // ㏙
    /// `\u{33D9}`: ppm in CJK compatibility character.
    ("\u{33D9}", (Conv(Frac::from_exp10(-6), ZERO), DynDim::new(Dimensionless::CODE))),
    ("ppb", (Conv(Frac::from_exp10(-9), ZERO), DynDim::new(Dimensionless::CODE))),
    ("ppt", (Conv(Frac::from_exp10(-12), ZERO), DynDim::new(Dimensionless::CODE))),
    ("ppq", (Conv(Frac::from_exp10(-15), ZERO), DynDim::new(Dimensionless::CODE))),
    // °
    /// `\u{00B0}`: Degree (angle).
    ("\u{00B0}", (
        Conv(Frac::from_ratio(1, 360, 0), ZERO),
        DynDim::new(Dimensionless::CODE))
    ),
    // ˚
    /// `\u{02DA}`: Degree (angle).
    ("\u{02DA}", (
        Conv(Frac::from_ratio(1, 360, 0), ZERO),
        DynDim::new(Dimensionless::CODE))
    ),
    ("rad", (RAD2BASE, DynDim::new(Dimensionless::CODE))),
    // ㎭
    /// `\u{33AD}`: Radian in CJK compatibility character.
    ("\u{33AD}", (RAD2BASE, DynDim::new(Dimensionless::CODE))),
    ("sr", (SR2BASE, DynDim::new(Dimensionless::CODE))),
    // ㏛
    /// `\u{33DB}`: Steradian in CJK compatibility character.
    ("\u{33DB}", (SR2BASE, DynDim::new(Dimensionless::CODE))),
    ("rpm", (Conv(Frac::from_ratio(1, 60, 0), ZERO), DynDim::new(Frequency::CODE))),
    // L
    /// Metre (meter). The base unit of length.
    ("m", (Conv(ONE, ZERO), DynDim::new(Length::CODE))),
    // ㍍
    /// `\u{334D}`: Meter in CJK compatibility character.
    ("\u{334D}", (Conv(ONE, ZERO), DynDim::new(Length::CODE))),
    // ㌖
    /// `\u{3316}`: Kilo metre in CJK compatibility character.
    ("\u{3316}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(Length::CODE))),
    // ㍷
    /// `\u{3377}`: Deci metre in CJK compatibility character.
    ("\u{3377}", (Conv(Frac::from_exp10(-1), ZERO), DynDim::new(Length::CODE))),
    // ㎙
    /// `\u{3399}`: Femto metre in CJK compatibility character.
    ("\u{3399}", (Conv(Frac::from_exp10(-12), ZERO), DynDim::new(Length::CODE))),
    // ㎚
    /// `\u{339A}`: Nano metre in CJK compatibility character.
    ("\u{339A}", (Conv(Frac::from_exp10(-9), ZERO), DynDim::new(Length::CODE))),
    // ㎛
    /// `\u{339B}`: Micro metre in CJK compatibility character.
    ("\u{339B}", (Conv(Frac::from_exp10(-6), ZERO), DynDim::new(Length::CODE))),
    // ㎜
    /// `\u{339C}`: Milli metre in CJK compatibility character.
    ("\u{339C}", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(Length::CODE))),
    // ㎝
    /// `\u{339D}`: Centi metre in CJK compatibility character.
    ("\u{339D}", (Conv(Frac::from_exp10(-2), ZERO), DynDim::new(Length::CODE))),
    // ㎞
    /// `\u{339E}`: Kilo metre in CJK compatibility character.
    ("\u{339E}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(Length::CODE))),
    /// Inch. The period is needed to prevent recognizing 'min' as milli inch.
    ("in.", (INCH2METER, DynDim::new(Length::CODE))),
    // ㌅
    /// `\u{3305}`: Inch in CJK compatibility character.
    ("\u{3305}", (INCH2METER, DynDim::new(Length::CODE))),
    // ㏌
    /// `\u{33CC}`: Inch in CJK compatibility character.
    ("\u{33CC}", (INCH2METER, DynDim::new(Length::CODE))),
    /// The period is needed to prevent conflicting with femto ton (ft)
    ("ft.", (FOOT2INCH.then(INCH2METER), DynDim::new(Length::CODE))),
    // ㌳
    /// `\u{3333}`: Foot in CJK compatibility character.
    ("\u{3333}", (FOOT2INCH.then(INCH2METER), DynDim::new(Length::CODE))),
    /// Yard. Because foot and inch need a period, the yard also requires it to prevent confusion.
    ("yd.", (YARD2FOOT.then(FOOT2INCH).then(INCH2METER), DynDim::new(Length::CODE))),
    // ㍎
    /// `\u{334E}`: Yard in CJK compatibility character.
    ("\u{334E}", (YARD2FOOT.then(FOOT2INCH).then(INCH2METER), DynDim::new(Length::CODE))),
    // ㍏
    /// `\u{334F}`: Yard in CJK compatibility character.
    ("\u{334F}", (YARD2FOOT.then(FOOT2INCH).then(INCH2METER), DynDim::new(Length::CODE))),
    // Å
    /// `\u{00C5}`: [Angstrom](https://en.wikipedia.org/wiki/Angstrom)
    ("\u{00C5}", (Conv(Frac::from_exp10(-10), ZERO), DynDim::new(Length::CODE))),
    // Å
    /// `\u{212B}`: [Angstrom](https://en.wikipedia.org/wiki/Angstrom)
    ("\u{212B}", (Conv(Frac::from_exp10(-10), ZERO), DynDim::new(Length::CODE))),
    // ㌋
    /// `\u{330B}`: Nautical mile in CJK compatibility character.
    ("\u{330B}", (Conv(Frac::from_int(1852), ZERO), DynDim::new(Length::CODE))),
    // 海里
    /// `\u{6D77}\u{91CC}`: Nautical mile in hanzi (kanji).
    ("\u{6D77}\u{91CC}", (Conv(Frac::from_int(1852), ZERO), DynDim::new(Length::CODE))),
    ("mile", (
        Conv(Frac::from_int(1760), ZERO).then(YARD2FOOT).then(FOOT2INCH).then(INCH2METER),
        DynDim::new(Length::CODE)
    )),
    // ㍄
    /// `\u{3344}`: Mile in CJK compatibility character.
    ("\u{3344}", (
        Conv(Frac::from_int(1760), ZERO).then(YARD2FOOT).then(FOOT2INCH).then(INCH2METER),
        DynDim::new(Length::CODE)
    )),
    // ㏕
    /// `\u{33D5}`: Mile in CJK compatibility character.
    ("\u{33D5}", (
        Conv(Frac::from_int(1760), ZERO).then(YARD2FOOT).then(FOOT2INCH).then(INCH2METER),
        DynDim::new(Length::CODE)
    )),
    /// [Astronomical unit](https://en.wikipedia.org/wiki/Astronomical_unit).
    ("au", (Conv(Frac::from_int(149_597_870_700), ZERO), DynDim::new(Length::CODE))),
    // ㍳
    /// `\u{3373}`: [Astronomical unit](https://en.wikipedia.org/wiki/Astronomical_unit) in CJK compatibility character.
    ("\u{3373}", (Conv(Frac::from_int(149_597_870_700), ZERO), DynDim::new(Length::CODE))),
    /// [Parsec](https://en.wikipedia.org/wiki/Parsec).
    /// The period is needed to prevent conflicting the light speed (`c`).
    ("pc.", (
        Conv(Frac::from_ratio(3_085_677_581, 1, 7), ZERO),
        DynDim::new(Length::CODE)
    )),
    // ㍶
    /// `\u{3376}`: [Parsec](https://en.wikipedia.org/wiki/Parsec) in CJK compatibility character.
    ("\u{3376}", (
        Conv(Frac::from_ratio(3_085_677_581, 1, 7), ZERO),
        DynDim::new(Length::CODE)
    )),
    // M
    /// Gram. The base unit of mass.
    /// The [SI Standards] adopts the kilogram (kg) as the base unit of mass.
    /// But this definition adopts the gram (g) for ease of parse.
    ("g", (Conv(ONE, ZERO), DynDim::new(Mass::CODE))),
    // ㌘
    /// `\u{3318}`: Gram in CJK compatibility character.
    ("\u{3318}", (Conv(ONE, ZERO), DynDim::new(Mass::CODE))),
    // ㌕
    /// `\u{3315}`: Kilo gram in CJK compatibility character.
    ("\u{3315}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(Mass::CODE))),
    // ㎍
    /// `\u{338D}`: Micro gram in CJK compatibility character.
    ("\u{338D}", (Conv(Frac::from_exp10(-6), ZERO), DynDim::new(Mass::CODE))),
    // ㎎
    /// `\u{338E}`: Milli gram in CJK compatibility character.
    ("\u{338E}", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(Mass::CODE))),
    // ㎏
    /// `\u{338F}`: Kilo gram in CJK compatibility character.
    ("\u{338F}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(Mass::CODE))),
    ("t", (Conv(Frac::from_exp10(6), ZERO), DynDim::new(Mass::CODE))),
    // ㌙
    /// `\u{3319}`: Metric ton in CJK compatibility character.
    ("\u{3319}", (Conv(Frac::from_exp10(6), ZERO), DynDim::new(Mass::CODE))),
    // ㌧
    /// `\u{3327}`: Tonne in CJK compatibility character. Same as metric ton.
    ("\u{3327}", (Conv(Frac::from_exp10(6), ZERO), DynDim::new(Mass::CODE))),
    // ㍌
    /// `\u{334C}`: Megatonne in CJK compatibility character.
    ("\u{334C}", (Conv(Frac::from_exp10(9), ZERO), DynDim::new(Mass::CODE))),
    /// [Dalton](https://en.wikipedia.org/wiki/Dalton_(unit)).
    /// This definition exclude `u`(unified atomic mass unit) to prevent conflicting
    /// [Astronomical unit](https://en.wikipedia.org/wiki/Astronomical_unit) (`au`).
    ("Da", (
        Conv(Frac::from_ratio(1_660_539_066_60, 1, -35), ZERO),
        DynDim::new(Mass::CODE)
    )),
    ("lb", (POUND2GRAM, DynDim::new(Mass::CODE))),
    // ㍀
    /// `\u{3340}`: Pound in CJK compatibility character.
    ("\u{3340}", (POUND2GRAM, DynDim::new(Mass::CODE))),
    /// Ounce
    ("oz", (
        Conv(Frac::from_ratio(1, 16, 0), ZERO).mul(POUND2GRAM),
        DynDim::new(Mass::CODE))
    ),
    // ㌉
    /// `\u{3309}`: Ounce in CJK compatibility character.
    ("\u{3309}", (
        Conv(Frac::from_ratio(1, 16, 0), ZERO).mul(POUND2GRAM),
        DynDim::new(Mass::CODE))
    ),
    // ㌌
    /// `\u{330C}`: [Carat](https://en.wikipedia.org/wiki/Carat_(mass)) in CJK compatibility character.
    ("\u{330C}", (Conv(Frac::from_ratio(1, 5, 0), ZERO), DynDim::new(Mass::CODE))),
    // T
    /// Second. Base unit of Time.
    ("s", (Conv(ONE, ZERO), DynDim::new(Time::CODE))),
    /// Minute.
    ("min", (Conv(Frac::from_int(60), ZERO), DynDim::new(Time::CODE))),
    /// Hour.
    ("h", (Conv(Frac::from_int(60 * 60), ZERO), DynDim::new(Time::CODE))),
    /// Day.
    ("day", (Conv(Frac::from_int(60 * 60 * 24), ZERO), DynDim::new(Time::CODE))),
    /// [Julian year](https://en.wikipedia.org/wiki/Julian_year_(astronomy))
    ("year", (Conv(Frac::from_int(31_557_600), ZERO), DynDim::new(Time::CODE))),
    // 秒
    /// `\u{79D2}`: Second in hanzi (kanji).
    ("\u{79D2}", (Conv(ONE, ZERO), DynDim::new(Time::CODE))),
    // 分
    /// `\u{5206}`: Minute in hanzi (kanji).
    ("\u{5206}", (Conv(Frac::from_int(60), ZERO), DynDim::new(Time::CODE))),
    // 時
    /// `\u{6642}`: Hour in hanzi (kanji).
    ("\u{6642}", (Conv(Frac::from_int(60 * 60), ZERO), DynDim::new(Time::CODE))),
    // 日
    /// `\u{65E5}`: Day in hanzi (kanji).
    ("\u{65E5}", (Conv(Frac::from_int(60 * 60 * 24), ZERO), DynDim::new(Time::CODE))),
    // 年
    /// `\u{5E74}`: Year in hanzi (kanji).
    ("\u{5E74}", (Conv(Frac::from_int(31_557_600), ZERO), DynDim::new(Time::CODE))),
    // ㎰
    /// `\u{33B0}`: Pico second in CJK compatibility character.
    ("\u{33B0}", (Conv(Frac::from_exp10(-12), ZERO), DynDim::new(Time::CODE))),
    // ㎱
    /// `\u{33B1}`: Nano second in CJK compatibility character.
    ("\u{33B1}", (Conv(Frac::from_exp10(-9), ZERO), DynDim::new(Time::CODE))),
    // ㎲
    /// `\u{33B2}`: Micro second in CJK compatibility character.
    ("\u{33B2}", (Conv(Frac::from_exp10(-6), ZERO), DynDim::new(Time::CODE))),
    // ㎳
    /// `\u{33B3}`: Milli second in CJK compatibility character.
    ("\u{33B3}", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(Time::CODE))),
    // θ
    /// [Kelvin](https://en.wikipedia.org/wiki/Kelvin).
    /// The base unit of temperature.
    ("K", (Conv(ONE, ZERO), DynDim::new(Temperature::CODE))),
    // K
    /// `\u{212A}`: [Kelvin](https://en.wikipedia.org/wiki/Kelvin).
    /// The base unit of temperature.
    ("\u{212A}", (Conv(ONE, ZERO), DynDim::new(Temperature::CODE))),
    // °R
    /// `\u{00B0}` and `R`: The [degrees Rankine](https://en.wikipedia.org/wiki/Rankine_scale).
    ("\u{00B0}R", (RANKINE2KELVIN, DynDim::new(Temperature::CODE))),
    // °F
    /// `\u{00B0}` and `F`: The [degree Fahrenheit](https://en.wikipedia.org/wiki/Fahrenheit).
    ("\u{00B0}F", (FAHRENHEIT2CELSIUS.then(CELSIUS2KELVIN), DynDim::new(Temperature::CODE))),
    // ℉
    /// `\u{2109}`: The [degree Fahrenheit](https://en.wikipedia.org/wiki/Fahrenheit).
    ("\u{2109}", (FAHRENHEIT2CELSIUS.then(CELSIUS2KELVIN), DynDim::new(Temperature::CODE))),
    // °C
    /// `\u{00B0}` and `C`: The [degree Celsius](https://en.wikipedia.org/wiki/Celsius).
    ("\u{00B0}C", (CELSIUS2KELVIN, DynDim::new(Temperature::CODE))),
    // ℃
    /// `\u{2103}`: The [degree Celsius](https://en.wikipedia.org/wiki/Celsius).
    ("\u{2103}", (CELSIUS2KELVIN, DynDim::new(Temperature::CODE))),
    // N
    /// Mole. The base unit of amount of substance.
    ("mol", (Conv(ONE, ZERO), DynDim::new(Amount::CODE))),
    // ㏖
    /// `\u{33D6}`: Mole in CJK compatibility character.
    ("\u{33D6}", (Conv(ONE, ZERO), DynDim::new(Amount::CODE))),
    // I
    /// [Ampere](https://en.wikipedia.org/wiki/Ampere).
    /// The base unit of electric current.
    ("A", (Conv(ONE, ZERO), DynDim::new(Current::CODE))),
    // ㌂
    /// `\u{3302}`: Ampere in CJK compatibility character.
    ("\u{3302}", (Conv(ONE, ZERO), DynDim::new(Current::CODE))),
    // ㎀
    /// `\u{3380}`: Pico ampere in CJK compatibility character.
    ("\u{3380}", (Conv(Frac::from_exp10(-12), ZERO), DynDim::new(Current::CODE))),
    // ㎁
    /// `\u{3381}`: Nano ampere in CJK compatibility character.
    ("\u{3381}", (Conv(Frac::from_exp10(-9), ZERO), DynDim::new(Current::CODE))),
    // ㎂
    /// `\u{3382}`: Micro ampere in CJK compatibility character.
    ("\u{3382}", (Conv(Frac::from_exp10(-6), ZERO), DynDim::new(Current::CODE))),
    // ㎃
    /// `\u{3383}`: Milli ampere in CJK compatibility character.
    ("\u{3383}", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(Current::CODE))),
    // ㎄
    /// `\u{3384}`: Kilo ampere in CJK compatibility character.
    ("\u{3384}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(Current::CODE))),
    // ㏟
    /// `\u{33DF}`: Ampere par metre in CJK compatibility character.
    ("\u{33DF}", (Conv(ONE, ZERO), DynDim::new(MagneticFieldStrength::CODE))),
    // J
    /// [Candela](https://en.wikipedia.org/wiki/Candela).
    /// The base unit of luminous intensity.
    ("cd", (Conv(ONE, ZERO), DynDim::new(Luminous::CODE))),
    // ㏅
    /// `\u{33C5}`: [Candela](https://en.wikipedia.org/wiki/Candela) in CJK compatibility character.
    ("\u{33C5}", (Conv(ONE, ZERO), DynDim::new(Luminous::CODE))),
    /// [Lumen](https://en.wikipedia.org/wiki/Lumen_(unit)).
    ("lm", (SR2BASE, DynDim::new(Luminous::CODE))),
    // ㏐
    /// `\u{33D0}`: [Lumen](https://en.wikipedia.org/wiki/Lumen_(unit)) in CJK compatibility character.
    ("\u{33D0}", (SR2BASE, DynDim::new(Luminous::CODE))),
    /// [Lux](https://en.wikipedia.org/wiki/Lux).
    ("lx", (SR2BASE, DynDim::new(Illuminance::CODE))),
    // ㏓
    /// `\u{33D3}`: [Lux](https://en.wikipedia.org/wiki/Lux) in CJK compatibility character.
    ("\u{33D3}", (SR2BASE, DynDim::new(Illuminance::CODE))),
    // Frequency
    ("Hz", (Conv(ONE, ZERO), DynDim::new(Frequency::CODE))),
    // ㌹
    /// `\u{3339}`: Hz in CJK compatibility character.
    ("\u{3339}", (Conv(ONE, ZERO), DynDim::new(Frequency::CODE))),
    // ㎐
    /// `\u{3339}`: Hz in CJK compatibility character.
    ("\u{3339}", (Conv(ONE, ZERO), DynDim::new(Frequency::CODE))),
    // ㎑
    /// `\u{3339}`: Kilo Hz in CJK compatibility character.
    ("\u{3339}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(Frequency::CODE))),
    // ㎒
    /// `\u{3339}`: Mega Hz in CJK compatibility character.
    ("\u{3339}", (Conv(Frac::from_exp10(6), ZERO), DynDim::new(Frequency::CODE))),
    // ㎓
    /// `\u{3339}`: Giga Hz in CJK compatibility character.
    ("\u{3339}", (Conv(Frac::from_exp10(9), ZERO), DynDim::new(Frequency::CODE))),
    // ㎔
    /// `\u{3339}`: Tera Hz in CJK compatibility character.
    ("\u{3339}", (Conv(Frac::from_exp10(12), ZERO), DynDim::new(Frequency::CODE))),
    /// [Becquerel](https://en.wikipedia.org/wiki/Becquerel).
    ("Bq", (Conv(ONE, ZERO), DynDim::new(Frequency::CODE))),
    // ㏃
    /// `\u{33C3}`: [Becquerel](https://en.wikipedia.org/wiki/Becquerel) in CJK compatibility character.
    ("\u{33C3}", (Conv(ONE, ZERO), DynDim::new(Frequency::CODE))),
    /// [Curie](https://en.wikipedia.org/wiki/Curie_(unit)).
    ("Ci", (Conv(Frac::from_ratio(37, 1, 9), ZERO), DynDim::new(Frequency::CODE))),
    // ㌒
    /// `\u{3312}`: [Curie](https://en.wikipedia.org/wiki/Curie_(unit)) in CJK compatibility character.
    ("\u{3312}", (
        Conv(Frac::from_ratio(37, 1, 9), ZERO),
        DynDim::new(Frequency::CODE)
    )),
    // ㌟
    /// `\u{331F}`: Cycle in CJK compatibility character.
    ("\u{331F}", (Conv(ONE, ZERO), DynDim::new(Dimensionless::CODE))),
    // ㎮
    /// `\u{33AE}`: rad/s in CJK compatibility character.
    ("\u{33AE}", (RAD2BASE, DynDim::new(Frequency::CODE))),
    // ㎯
    /// `\u{33AF}`: rad/s<sup>2</sup> in CJK compatibility character.
    ("\u{33AF}", (RAD2BASE, DynDim::new(FrequencyAcceleration::CODE))),
    // Area
    // ㍸
    /// `\u{3378}`: dm<sup>2</sup> in CJK compatibility character.
    ("\u{3378}", (Conv(Frac::from_exp10(-2), ZERO), DynDim::new(Area::CODE))),
    // ㎟
    /// `\u{339F}`: mm<sup>2</sup> in CJK compatibility character.
    ("\u{339F}", (Conv(Frac::from_exp10(-6), ZERO), DynDim::new(Area::CODE))),
    // ㎠
    /// `\u{33A0}`: cm<sup>2</sup> in CJK compatibility character.
    ("\u{33A0}", (Conv(Frac::from_exp10(-4), ZERO), DynDim::new(Area::CODE))),
    // ㎡
    /// `\u{33A1}`: m<sup>2</sup> in CJK compatibility character.
    ("\u{33A1}", (Conv(ONE, ZERO), DynDim::new(Area::CODE))),
    // ㎢
    /// `\u{33A2}`: km<sup>2</sup> in CJK compatibility character.
    ("\u{33A2}", (Conv(Frac::from_exp10(6), ZERO), DynDim::new(Area::CODE))),
    /// Are.
    /// To prevent conflicting Pascal (Pa), it needs a period.
    ("a.", (Conv(Frac::from_exp10(2), ZERO), DynDim::new(Area::CODE))),
    /// `\u{0430}`: CYRILLIC SMALL LETTER A.
    /// If you really don't want to put a period in are, use this one.
    ("\u{0430}", (Conv(Frac::from_exp10(2), ZERO), DynDim::new(Area::CODE))),
    // ㌃
    /// `\u{3303}`: Are in CJK compatibility character.
    ("\u{3303}", (Conv(Frac::from_exp10(2), ZERO), DynDim::new(Area::CODE))),
    // ㌶
    /// `\u{3336}`: Hecto are in CJK compatibility character.
    ("\u{3336}", (Conv(Frac::from_exp10(4), ZERO), DynDim::new(Area::CODE))),
    // ㏊
    /// `\u{33CA}`: Hecto are in CJK compatibility character.
    ("\u{33CA}", (Conv(Frac::from_exp10(4), ZERO), DynDim::new(Area::CODE))),
    ("acre", (
        ACRE2FT2.mul(FOOT2INCH).mul(FOOT2INCH).mul(INCH2METER).mul(INCH2METER),
        DynDim::new(Area::CODE)
    )),
    // ㌈
    /// `\u{3308}`: Acre in CJK compatibility character.
    ("\u{3308}",
        (ACRE2FT2.mul(FOOT2INCH).mul(FOOT2INCH).mul(INCH2METER).mul(INCH2METER),
        DynDim::new(Area::CODE))
    ),
    // Volume
    /// Litre (Liter)
    ("l", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(Volume::CODE))),
    /// Litre (Liter)
    ("L", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(Volume::CODE))),
    // ℓ
    /// `\u{2113}`: Litre in CJK compatibility character.
    ("\u{2113}", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(Volume::CODE))),
    // ㎕
    /// `\u{3395}`: Micro litre in CJK compatibility character.
    ("\u{3395}", (Conv(Frac::from_exp10(-9), ZERO), DynDim::new(Volume::CODE))),
    // ㎖
    /// `\u{3396}`: Milli litre in CJK compatibility character.
    ("\u{3396}", (Conv(Frac::from_exp10(-6), ZERO), DynDim::new(Volume::CODE))),
    // ㎗
    /// `\u{3397}`: Deci litre in CJK compatibility character.
    ("\u{3397}", (Conv(Frac::from_exp10(-4), ZERO), DynDim::new(Volume::CODE))),
    // ㎘
    /// `\u{3398}`: Kilo litre in CJK compatibility character.
    ("\u{3398}", (Conv(ONE, ZERO), DynDim::new(Volume::CODE))),
    // ㍹
    /// `\u{3379}`: dm<sup>3</sup> in CJK compatibility character.
    ("\u{3379}", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(Volume::CODE))),
    // ㎣
    /// `\u{33A3}`: mm<sup>3</sup> in CJK compatibility character.
    ("\u{33A3}", (Conv(Frac::from_exp10(-9), ZERO), DynDim::new(Volume::CODE))),
    // ㎤
    /// `\u{33A4}`: cm<sup>3</sup> in CJK compatibility character.
    ("\u{33A4}", (Conv(Frac::from_exp10(-6), ZERO), DynDim::new(Volume::CODE))),
    // ㏄
    /// `\u{33C4}`: Cubic centimeter in CJK compatibility character.
    ("\u{33C4}", (Conv(Frac::from_exp10(-6), ZERO), DynDim::new(Volume::CODE))),
    // ㎥
    /// `\u{33A5}`: m<sup>3</sup> in CJK compatibility character.
    ("\u{33A5}", (Conv(ONE, ZERO), DynDim::new(Volume::CODE))),
    // ㎦
    /// `\u{33A6}`: km<sup>3</sup> in CJK compatibility character.
    ("\u{33A6}", (Conv(Frac::from_exp10(9), ZERO), DynDim::new(Volume::CODE))),
    /// Imperial gallon
    ("gal.en", (
        Conv(Frac::from_ratio(4_546_09, 1, -8), ZERO),
        DynDim::new(Volume::CODE)
    )),
    /// U.S. fluid gallon
    ("gal.us", (
        Conv(Frac::from_ratio(3_785_412, 1, -9), ZERO),
        DynDim::new(Volume::CODE)
    )),
    // ㌎
    /// `\u{330E}`: U.S. fluid gallon in CJK compatibility character.
    ("\u{330E}", (
        Conv(Frac::from_ratio(3_785_412, 1, -9), ZERO),
        DynDim::new(Volume::CODE)
    )),
    /// Barrel
    ("bbl", (
        Conv(Frac::from_ratio(158_987_294_928, 1, -12), ZERO),
        DynDim::new(Volume::CODE)
    )),
    // ㌭
    /// `\u{332D}`: Barrel in CJK compatibility character.
    ("\u{332D}", (
        Conv(Frac::from_ratio(158_987_294_928, 1, -12), ZERO),
        DynDim::new(Volume::CODE)
    )),
    // Velocity
    /// The speed of light in vacuum.
    ("c", (Conv(Frac::from_int(299_792_458), ZERO), DynDim::new(Velocity::CODE))),
    /// The speed of light in vacuum.
    /// This can describe astronomical length by combining with time units.
    /// (e.g. "light year")
    ("light", (Conv(Frac::from_int(299_792_458), ZERO), DynDim::new(Velocity::CODE))),
    // 光
    /// `\u{5149}`: The speed of light in vacuum.
    /// This can describe astronomical length by combining with time units.
    /// Hanji (kanji) does not use a space character to delimit word ordinary.
    /// Zero width space is suitable for this use. (e.g. 光​年)
    ("\u{5149}", (Conv(Frac::from_int(299_792_458), ZERO), DynDim::new(Velocity::CODE))),
    // ㎧
    /// `\u{33A7}`: m/s in CJK compatibility character.
    ("\u{33A7}", (Conv(ONE, ZERO), DynDim::new(Velocity::CODE))),
    /// Knot. This definition exclude `kt` to prevent conflicting with kilo ton (kt).
    ("kn", (Conv(Frac::from_ratio(1852, 3600, 0), ZERO), DynDim::new(Velocity::CODE))),
    // ㌩
    /// `\u{3329}`: Knot in CJK compatibility character.
    ("\u{3329}", (
        Conv(Frac::from_ratio(1852, 3600, 0), ZERO),
        DynDim::new(Velocity::CODE)
    )),
    // ㏏
    /// `\u{33CF}`: Knot in CJK compatibility character.
    ("\u{33CF}", (
        Conv(Frac::from_ratio(1852, 3600, 0), ZERO),
        DynDim::new(Velocity::CODE)
    )),
    // Acceleration
    // ㎨
    /// `\u{33A8}`: m/s<sup>2</sup> in CJK compatibility character.
    ("\u{33A8}", (Conv(ONE, ZERO), DynDim::new(Acceleration::CODE))),
    /// [Gal](https://en.wikipedia.org/wiki/Gal_(unit)). Not gallon.
    ("Gal", (Conv(Frac::from_exp10(-2), ZERO), DynDim::new(Velocity::CODE))),
    // ㏿
    /// `\u{33FF}`: [Gal](https://en.wikipedia.org/wiki/Gal_(unit)) in CJK compatibility character.
    ("\u{33FF}", (Conv(Frac::from_exp10(-2), ZERO), DynDim::new(Velocity::CODE))),
    // Force
    /// Newton
    ("N", (NEWTON2BASE, DynDim::new(Force::CODE))),
    /// Gram force.
    ("gf", (GRAM_FORCE2NEWTON.then(NEWTON2BASE), DynDim::new(Force::CODE))),
    /// Pound force.
    ("lbf", (POUND2GRAM.then(GRAM_FORCE2NEWTON).then(NEWTON2BASE), DynDim::new(Force::CODE))),
    // Pressure
    /// Pascal.
    ("Pa", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(Pressure::CODE))),
    // ㎩
    /// `\u{33A9}`: Pascal in CJK compatibility character.
    ("\u{33A9}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(Pressure::CODE))),
    // ㍱
    /// `\u{3371}`: Hecto Pascal in CJK compatibility character.
    ("\u{3371}", (Conv(Frac::from_exp10(5), ZERO), DynDim::new(Pressure::CODE))),
    // ㎪
    /// `\u{33AA}`: Kilo Pascal in CJK compatibility character.
    ("\u{33AA}", (Conv(Frac::from_exp10(6), ZERO), DynDim::new(Pressure::CODE))),
    // ㎫
    /// `\u{33AB}`: Mega Pascal in CJK compatibility character.
    ("\u{33AB}", (Conv(Frac::from_exp10(9), ZERO), DynDim::new(Pressure::CODE))),
    // ㎬
    /// `\u{33AC}`: Giga Pascal in CJK compatibility character.
    ("\u{33AC}", (Conv(Frac::from_exp10(12), ZERO), DynDim::new(Pressure::CODE))),
    ("bar", (Conv(Frac::from_exp10(8), ZERO), DynDim::new(Pressure::CODE))),
    // ㍴
    /// `\u{3374}`: bar in CJK compatibility character.
    ("\u{3374}", (Conv(Frac::from_exp10(8), ZERO), DynDim::new(Pressure::CODE))),
    // ㍊
    /// `\u{334A}`: Milli bar in CJK compatibility character.
    ("\u{334A}", (Conv(Frac::from_exp10(5), ZERO), DynDim::new(Pressure::CODE))),
    // ㏔
    /// `\u{33D4}`: Milli bar in CJK compatibility character.
    ("\u{33D4}", (Conv(Frac::from_exp10(5), ZERO), DynDim::new(Pressure::CODE))),
    /// at (kgf/cm<sup>2</sup>). It needs period to prevent conflicting atto ton (at).
    ("at.", (AT2BASE, DynDim::new(Pressure::CODE))),
    /// Absolute at. Same as kgf/cm<sup>2</sup>\[abs\].
    ("ata", (AT2BASE, DynDim::new(Pressure::CODE))),
    /// Gaged at. Same as kgf/cm<sup>2</sup>\[gage\].
    ("atg", (Conv(AT2BASE.0, Frac::from_int(101325000)), DynDim::new(Pressure::CODE))),
    /// Pound force per square inch (lbf/in.<sup>2</sup>).
    ("psi", (PSI2BASE, DynDim::new(Pressure::CODE))),
    /// Absolute pound force per square inch (lbf/in.<sup>2</sup>\[abs\]).
    ("psia", (PSI2BASE,DynDim::new(Pressure::CODE))),
    /// Gaged pound force per square inch (lbf/in.<sup>2</sup>\[gage\]).
    ("psig", (Conv(PSI2BASE.0, Frac::from_int(101325000)), DynDim::new(Pressure::CODE))),
    /// Pressure based mercury head. lacked length dimension. (see [Head](../unit/index.html#head))
    ("Hg", (Conv(Frac::from_ratio(101325, 760, 3), ZERO), DynDim::new(Head::CODE))),
    // ㋌
    /// `\u{32CC}`: Hg in CJK compatibility character. (see [Head](../unit/index.html#head))
    ("\u{32CC}", (
        Conv(Frac::from_ratio(101325, 760, 3), ZERO),
        DynDim::new(Head::CODE)
    )),
    /// Pressure based water head. lacked length dimension. (see [Head](../unit/index.html#head))
    ("Aq", (Conv(Frac::from_int(9806650), ZERO), DynDim::new(Head::CODE))),
    // Energy
    /// [Joule](https://en.wikipedia.org/wiki/Joule).
    ("J", (JOULE2BASE, DynDim::new(Energy::CODE))),
    /// [Calorie](https://en.wikipedia.org/wiki/Calorie).
    ("cal", (CALORIE2JOULE.then(JOULE2BASE), DynDim::new(Energy::CODE))),
    // ㌍
    /// `\u{330D}`: [Calorie](https://en.wikipedia.org/wiki/Calorie) in CJK compatibility character.
    ("\u{330D}", (CALORIE2JOULE.then(JOULE2BASE), DynDim::new(Energy::CODE))),
    // ㎈
    /// `\u{3388}`: [Calorie](https://en.wikipedia.org/wiki/Calorie) in CJK compatibility character.
    ("\u{3388}", (CALORIE2JOULE.then(JOULE2BASE), DynDim::new(Energy::CODE))),
    // ㎉
    /// `\u{3389}`: Kilo [Calorie](https://en.wikipedia.org/wiki/Calorie) in CJK compatibility character.
    ("\u{3389}", (
        Conv(Frac::from_exp10(3), ZERO).then(CALORIE2JOULE).then(JOULE2BASE),
        DynDim::new(Energy::CODE)
    )),
    /// [Hartree atomic units](https://en.wikipedia.org/wiki/Hartree_atomic_units).
    ("E_h", (
        Conv(Frac::from_ratio(4_359_744_722_207_1, 1, -28), ZERO),
        DynDim::new(Energy::CODE)
    )),
    /// [British thermal unit](https://en.wikipedia.org/wiki/British_thermal_unit).
    ("Btu", (BTU2CALORIE.then(CALORIE2JOULE).then(JOULE2BASE), DynDim::new(Energy::CODE))),
    /// [Erg](https://en.wikipedia.org/wiki/Erg).
    ("erg", (Conv(Frac::from_exp10(-4), ZERO), DynDim::new(Energy::CODE))),
    // ㋍
    /// `\u{32CD}`: [Erg](https://en.wikipedia.org/wiki/Erg) in CJK compatibility character.
    ("\u{32CD}", (Conv(Frac::from_exp10(-4), ZERO), DynDim::new(Energy::CODE))),
    /// [Electronvolt](https://en.wikipedia.org/wiki/Electronvolt).
    ("eV", (
        Conv(Frac::from_ratio(1_602_176_634, 1, -25), ZERO),
        DynDim::new(Energy::CODE)
    )),
    // ㋎
    /// `\u{32CE}`: [Electronvolt](https://en.wikipedia.org/wiki/Electronvolt) in CJK compatibility character.
    ("\u{32CE}", (
        Conv(Frac::from_ratio(1_602_176_634, 1, -25), ZERO),
        DynDim::new(Energy::CODE)
    )),
    // Radiation
    /// [Gray](https://en.wikipedia.org/wiki/Gray_(unit)).
    ("Gy", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(SpecificEnthalpy::CODE))),
    // ㏉
    /// `\u{33C9}`: [Gray](https://en.wikipedia.org/wiki/Gray_(unit)) in CJK compatibility character.
    ("\u{33C9}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(SpecificEnthalpy::CODE))),
    /// [Sievert](https://en.wikipedia.org/wiki/Sievert).
    ("Sv", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(SpecificEnthalpy::CODE))),
    // ㏜
    /// `\u{33DC}`: [Sievert](https://en.wikipedia.org/wiki/Sievert) in CJK compatibility character.
    ("\u{33DC}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(SpecificEnthalpy::CODE))),
    // ㍕
    /// `\u{3355}`: [Rem](https://en.wikipedia.org/wiki/Roentgen_equivalent_man) in CJK compatibility character.
    ("\u{3355}", (Conv(Frac::from_exp10(1), ZERO), DynDim::new(SpecificEnthalpy::CODE))),
    // ㍖
    /// `\u{3356}`: [Roentgen](https://en.wikipedia.org/wiki/Roentgen_(unit)) in CJK compatibility character.
    ("\u{3356}", (
        Conv(Frac::from_ratio(258, 1, -5), ZERO),
        DynDim::new(RadiationExposure::CODE)
    )),
    // Power
    /// Watt.
    ("W", (WATT2BASE, DynDim::new(Power::CODE))),
    // ㍗
    /// `\u{3357}`: Watt in CJK compatibility character.
    ("\u{3357}", (WATT2BASE, DynDim::new(Power::CODE))),
    // ㌗
    /// `\u{3317}`: Kilo Watt in CJK compatibility character.
    ("\u{3317}", (Conv(Frac::from_exp10(6), ZERO), DynDim::new(Power::CODE))),
    // ㎺
    /// `\u{33BA}`: Pico Watt in CJK compatibility character.
    ("\u{33BA}", (Conv(Frac::from_exp10(-15), ZERO), DynDim::new(Power::CODE))),
    // ㎻
    /// `\u{33BB}`: Nano Watt in CJK compatibility character.
    ("\u{33BB}", (Conv(Frac::from_exp10(-12), ZERO), DynDim::new(Power::CODE))),
    // ㎼
    /// `\u{33BC}`: Micro Watt in CJK compatibility character.
    ("\u{33BC}", (Conv(Frac::from_exp10(-9), ZERO), DynDim::new(Power::CODE))),
    // ㎽
    /// `\u{33BD}`: Milli Watt in CJK compatibility character.
    ("\u{33BD}", (Conv(Frac::from_exp10(-6), ZERO), DynDim::new(Power::CODE))),
    // ㎾
    /// `\u{33BE}`: Kilo Watt in CJK compatibility character.
    ("\u{33BE}", (Conv(Frac::from_exp10(6), ZERO), DynDim::new(Power::CODE))),
    // ㎿
    /// `\u{33BF}`: Mega Watt in CJK compatibility character.
    ("\u{33BF}", (Conv(Frac::from_exp10(9), ZERO), DynDim::new(Power::CODE))),
    // ㏋
    /// `\u{33CB}`: Imperial horsepower in CJK compatibility character.
    ("\u{33CB}", (
        Conv(Frac::from_int(550), ZERO)
            .mul(POUND2GRAM).mul(GRAM_FORCE2NEWTON).mul(NEWTON2BASE).mul(FOOT2INCH).mul(INCH2METER),
        DynDim::new(Power::CODE)
    )),
    // Electrical voltage
    /// Volt.
    ("V", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(Voltage::CODE))),
    // ㏞
    /// `\u{33DE}`: V/m in CJK compatibility character.
    ("\u{33DE}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(ElectricFieldStrength::CODE))),
    // ㌾
    /// `\u{333E}`: Volt in CJK compatibility character.
    ("\u{333E}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(Voltage::CODE))),
    // ㎴
    /// `\u{33B4}`: Pico volt in CJK compatibility character.
    ("\u{33B4}", (Conv(Frac::from_exp10(-9), ZERO), DynDim::new(Voltage::CODE))),
    // ㎵
    /// `\u{33B5}`: Nano volt in CJK compatibility character.
    ("\u{33B5}", (Conv(Frac::from_exp10(-6), ZERO), DynDim::new(Voltage::CODE))),
    // ㎶
    /// `\u{33B6}`: Micro volt in CJK compatibility character.
    ("\u{33B6}", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(Voltage::CODE))),
    // ㎷
    /// `\u{33B7}`: Milli volt in CJK compatibility character.
    ("\u{33B7}", (Conv(ONE, ZERO), DynDim::new(Voltage::CODE))),
    // ㎸
    /// `\u{33B8}`: Kilo volt in CJK compatibility character.
    ("\u{33B8}", (Conv(Frac::from_exp10(6), ZERO), DynDim::new(Voltage::CODE))),
    // ㎹
    /// `\u{33B9}`: Mega volt in CJK compatibility character.
    ("\u{33B9}", (Conv(Frac::from_int(1000_000_000), ZERO), DynDim::new(Voltage::CODE))),
    // Electrical resistance
    // Ω
    /// `\u{03A9}`: Ohm.
    ("\u{03A9}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(ElectricResistance::CODE))),
    // Ω
    /// `\u{2126}`: Ohm.
    ("\u{2126}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(ElectricResistance::CODE))),
    // ㌊
    /// `\u{330A}`: Ohm in CJK compatibility character.
    ("\u{330A}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(ElectricResistance::CODE))),
    // ㏀
    /// `\u{33C0}`: Kilo ohm in CJK compatibility character.
    ("\u{33C0}", (Conv(Frac::from_exp10(6), ZERO), DynDim::new(ElectricResistance::CODE))),
    // ㏁
    /// `\u{33C1}`: Mega ohm in CJK compatibility character.
    ("\u{33C1}", (Conv(Frac::from_int(1000_000_000), ZERO), DynDim::new(ElectricResistance::CODE))),
    /// [Siemens](https://en.wikipedia.org/wiki/Siemens_(unit)).
    ("S", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(ElectricConductance::CODE))),
    // ℧
    /// `\u{2127}`: [Siemens](https://en.wikipedia.org/wiki/Siemens_(unit)).
    ("\u{2127}", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(ElectricConductance::CODE))),
    /// [Henry](https://en.wikipedia.org/wiki/Henry_(unit)).
    ("H", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(ElectricalInductance::CODE))),

    // Electrical Capacitance
    /// [Farad](https://en.wikipedia.org/wiki/Farad).
    ("F", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(Capacitance::CODE))),
    // ㌲
    /// `\u{3332}`: [Farad](https://en.wikipedia.org/wiki/Farad) in CJK compatibility character.
    ("\u{3332}", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(Capacitance::CODE))),
    // ㎊
    /// `\u{338A}`: Pico farad in CJK compatibility character.
    ("\u{338A}", (Conv(Frac::from_exp10(-15), ZERO), DynDim::new(Capacitance::CODE))),
    // ㎋
    /// `\u{338B}`: Nano farad in CJK compatibility character.
    ("\u{338B}", (Conv(Frac::from_exp10(-12), ZERO), DynDim::new(Capacitance::CODE))),
    // ㎌
    /// `\u{338C}`: Micro farad in CJK compatibility character.
    ("\u{338C}", (Conv(Frac::from_exp10(-9), ZERO), DynDim::new(Capacitance::CODE))),
    /// [Coulomb](https://en.wikipedia.org/wiki/Coulomb).
    ("C", (Conv(ONE, ZERO), DynDim::new(ElectricCharge::CODE))),
    // ㏆
    /// `\u{33C6}`: C/kg in CJK compatibility character.
    ("\u{33C6}", (Conv(Frac::from_exp10(-3), ZERO), DynDim::new(RadiationExposure::CODE))),
    /// [Weber](https://en.wikipedia.org/wiki/Weber_(unit)).
    ("Wb", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(MagneticFlux::CODE))),
    // ㏝
    /// `\u{33DD}`: [Weber](https://en.wikipedia.org/wiki/Weber_(unit)) in CJK compatibility character.
    ("\u{33DD}", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(MagneticFlux::CODE))),
    /// [Tesla](https://en.wikipedia.org/wiki/Tesla_(unit)).
    ("T", (Conv(Frac::from_exp10(3), ZERO), DynDim::new(MagneticFluxDensity::CODE))),
    /// [Poise](https://en.wikipedia.org/wiki/Poise_(unit)).
    ("P", (Conv(Frac::from_exp10(2), ZERO), DynDim::new(Viscosity::CODE))),
    /// [Stokes](https://en.wikipedia.org/wiki/Stokes_(unit)).
    ("St", (Conv(Frac::from_exp10(-4), ZERO), DynDim::new(KinematicViscosity::CODE))),
];

#[cfg(test)]
mod tests {
    extern crate std;

    use super::*;
    use std::eprintln;

    #[test]
    #[ignore]
    fn dump_def_table() {
        for (key, (cnv, dim)) in DEFAULT_UNIT_DEF.iter() {
            eprintln!("{} = {:?}, {}", key, cnv, dim);
        }
        panic!();
    }

    #[test]
    fn test_ata() {
        let cnv = GRAM_FORCE2NEWTON;
        let cnv = cnv.mul(NEWTON2BASE);
        let a = Conv(Frac::from_exp10(7), ZERO);
        let cnv = cnv.mul(a);

        assert_eq!(cnv.0.to_f64(), 98066500.0);
    }
}