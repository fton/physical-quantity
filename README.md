[![Latest Release](https://gitlab.com/fton/physical-quantity/-/badges/release.svg)](https://gitlab.com/fton/physical-quantity/-/releases)
[![pipeline status](https://gitlab.com/fton/physical-quantity/badges/main/pipeline.svg)](https://gitlab.com/fton/physical-quantity/-/commits/main)
[![coverage report](https://gitlab.com/fton/physical-quantity/badges/main/coverage.svg)](https://gitlab.com/fton/physical-quantity/-/commits/main)
# Description
Dimension and unit system for general physical quantities.

This package provids the implementations.
For general use, [unitage] is convenient.

When you use this package only for restrict the argument for your function to paticular dimension,
it may be prefer adding this package to the dependencies directly.
# Usage
```rust
use ::physical_quantity::*;

let inch: Unit<f64, _> = "in.".parse().unwrap();
let pq = inch.pq(1f64);
let mm: Unit<f64, _> = "mm".parse().unwrap();
let inch2mm = mm.value(pq).unwrap();

assert_eq!(inch2mm, 25.4);
```
# Features
This package has the following feature flags.
No feature flags are enabled by *default*.

- **`full`**
  Enables all features.
- **`default-units`**
  Enables the default definitions of the units.
  Enabling this flag enables `parser` flag also.
- **`parser`**
  Enables the unit string parser.
- **`ratio`**
  Enables [num_rational::BigRational] supports.
- **`std`**
  Enables [std] supports.

# Project status
This package is in the very early stage.

[std]: https://doc.rust-lang.org/std/index.html
[num_rational::BigRational]: https://docs.rs/num-rational/*/num_rational/type.BigRational.html
[unitage]: https://docs.rs/unitage/*/unitage/