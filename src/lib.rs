#![doc = include_str!("../README.md")]
#![cfg_attr(not(feature = "std"), no_std)]
#![allow(mixed_script_confusables)]

pub mod error;
mod dimension;
pub mod traits;
pub mod unit;
mod pq;
pub mod predefined;

pub use dimension:: { Dimension, Dim, DynDim };
pub use pq::PhysicalQuantity;
pub use unit:: { Unit, Conv };

#[cfg(test)]
mod tests {
    use super::*;
    use const_frac::Frac;
    use num::Float;

    #[test]
    fn test_triton_mass_in_u() {
        let u: Unit<Frac, _> = "Da".parse().unwrap();
        let v = Frac::from_ratio(3_015_500_716_21, 1, -11);
        let pq = u.pq(v);
        let float = u.value(pq).unwrap();

        assert_eq!(float.to_f64(), 3.015_500_716_21);
    }

    #[test]
    fn test_first_radiation_constant_for_spectral_radiance() {
        const FIRST_RADIATION_CONSTANT_FOR_SPECTRAL_RADIANCE: f64 = 1.191_042_972e-7;

        let u: Unit<Frac, _> = "W m2 sr-1".parse().unwrap();
        let v = Frac::from_ratio(1_191_042_972, 1, -16);
        let pq = u.pq(v);
        let v1 = u.value(pq).unwrap();
        let left =v1.to_f64();
        let right = v.to_f64();
        let (lmantissa, lexp, _) = left.integer_decode();
        let (rmantissa, rexp, _) = right.integer_decode();

        assert_eq!(left, right,
            "\u{1F623}\n({lmantissa:b}, {lexp})\n({rmantissa:b}, {rexp})\n");

        let (rmantissa, rexp, _) = FIRST_RADIATION_CONSTANT_FOR_SPECTRAL_RADIANCE.integer_decode();

        assert_eq!(left, FIRST_RADIATION_CONSTANT_FOR_SPECTRAL_RADIANCE,
            "\u{1F623}\n({lmantissa:b}, {lexp})\n({rmantissa:b}, {rexp})\n");
    }
}
